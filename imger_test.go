package aproxy

import (
	"bufio"
	"image/jpeg"
	"image/png"
	"io"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// borrowed from https://juststickers.in/wp-content/uploads/2016/07/go-programming-language.png

// TestGrayscaleImage gray scales an image and writes it out to a new file
// doesn't actually test if the gray scale works
func TestGrayscaleImagePng(t *testing.T) {

	const gopherTestPicture = "test/go-programming-language.png"
	const gopherTestPictureGrayscale = "test/go-programming-language-gs.png"

	f, err := os.Open(gopherTestPicture)
	assert.NoErrorf(t, err, "Could not open file %s", gopherTestPicture)
	defer f.Close()

	colorConfig, err := NewColorConfig(f)
	assert.NoErrorf(t, err, "Could not load config for file %s", gopherTestPicture)

	t.Logf("Config: %+v", colorConfig)
	f.Seek(0, io.SeekStart)

	img, err := GrayScaleImage(f)
	assert.NoError(t, err, "Error decoding image")
	assert.NotNil(t, img, "Img cannot be nil")

	// create new file and write it out
	gsf, err := os.Create(gopherTestPictureGrayscale)
	if err != nil && !os.IsExist(err) {
		assert.NoError(t, err, "Could not write file")
	}
	defer gsf.Close()

	// encode the PNG back out - lossless encoding
	png.Encode(bufio.NewWriter(gsf), img)
}

func TestGrayScaleImageJpeg(t *testing.T) {
	const gopherTestPicture = "test/go-programming-language.jpeg"
	const gopherTestPictureGrayscale = "test/go-programming-language-gs.jpeg"

	f, err := os.Open(gopherTestPicture)
	assert.NoErrorf(t, err, "Could not open file %s", gopherTestPicture)
	defer f.Close()

	colorConfig, err := NewColorConfig(f)
	assert.NoErrorf(t, err, "Could not load config for file %s", gopherTestPicture)

	t.Logf("Config: %+v", colorConfig)
	f.Seek(0, io.SeekStart)

	img, format, err := GrayScaleImageInPlace(f)
	assert.NoError(t, err, "Error decoding image")
	assert.NotNil(t, img, "Img cannot be nil")
	assert.Truef(t, strings.Compare("jpeg", format) == 0, "Image format %s did not match expected jpeg", format)

	// create new file and write it out
	gsf, err := os.Create(gopherTestPictureGrayscale)
	if err != nil && !os.IsExist(err) {
		assert.NoError(t, err, "Could not write file")
	}
	defer gsf.Close()

	// encode the jpeg back out - quality 100% here instead of default 75%
	jpeg.Encode(bufio.NewWriter(gsf), img, &jpeg.Options{Quality: 100})
}
