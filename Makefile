PKG=gitlab.com/cmbernard333/aproxy
EXE=aproxy
BIN_DIR:=$(GOPATH)/bin
GO_VERSION=$(shell go version)

$(info Using $(GO_VERSION))

all: aproxy

aproxy:
	go build -o $(EXE) cmd/server/main.go

.PHONY: test
test:
	go test -timeout 30s $(PKG)

.PHONY: clean
clean:
	rm -f $(EXE)

.PHONY: clobber
clobber: clean
	go clean -modcache -testcache
