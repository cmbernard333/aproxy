package main

import (
	"flag"
	"gitlab.com/cmbernard333/aproxy"
	"log"
	"os"
)

func main() {

	var sourceAddr string

	flag.StringVar(&sourceAddr, "source-addr", "", "Source proxy address")
	flag.Parse()

	// golang std log package is ok - exiting with Fatal* functions is kinda silly and no configurable log levels but this does the job

	log := log.New(os.Stdout, "proxy:", log.LstdFlags) // log to stdout with "proxy:", date, and time prefixed
	log.Printf("Initializing proxy server with origin %s", sourceAddr)

	if sourceAddr == "" {
		log.Fatalf("source-addr cannot be empty!")
	}

	p, err := aproxy.New(sourceAddr)
	if err != nil {
		log.Fatalf("error initializing proxy: %v", err)
	}

	err = p.Run()

	if err != nil {
		log.Fatalf("%+v", err)
	}
}
