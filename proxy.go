package aproxy

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

// Proxy represents the state and functionality for running the proxy server itself
type Proxy struct {
	Router      *mux.Router
	ProxyClient *ProxyClient
}

// New creates a new proxy object
func New(sourceAddr string) (p *Proxy, err error) {
	p = new(Proxy)
	p.Router = mux.NewRouter()
	p.ProxyClient, err = NewProxyClientWithAddr(sourceAddr)
	return p, err
}

// ServeHTTP satisfies the http.Handler interface
func (p *Proxy) proxyRouteHandler() http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		urlPath := strings.TrimPrefix(request.URL.Path, "/")
		log.Printf("Received %s from %s", urlPath, request.RemoteAddr)
		// NOTE: should probably also investigate the X-Forwarded-For/X-Real-Ip header BUT this proxy is just serving clients direct
		response, err := p.ProxyClient.ProxyGetRequest(urlPath, request.RemoteAddr, "http", request.RemoteAddr)
		if err != nil {
			log.Printf("Server: error: %v", err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		// close and cleanup
		defer func() {
			io.Copy(ioutil.Discard, response.Body)
			response.Body.Close()
		}()

		// let's convert to grayscale
		gsf, format, err := GrayScaleImageInPlace(response.Body)
		if err != nil {
			// bad request - we didn't download an image
			log.Printf("Server: Could not grayscale image: %v", err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		buffer := new(bytes.Buffer)
		if err := EncodeImage(buffer, gsf, format); err != nil {
			log.Printf("Server: Could not encode image: %v", err)
			writer.WriteHeader(http.StatusBadRequest)
			return
		}

		log.Printf("Writing grayscale image from %s to %s", urlPath, request.RemoteAddr)

		// write out the response headers
		for headerName, headerValue := range response.Header {
			writer.Header().Set(headerName, strings.Join(headerValue, ", "))
		}

		// stream response to the client - encode it back in the correct format
		// replace the content length as the gray scale image will probably be different
		writer.Header().Set("Content-Length", strconv.Itoa(buffer.Len()))
		io.Copy(writer, buffer)
	})
}

// Run starts the proxy server. It configures the origin of the proxy server using the sourceAddr
func (p *Proxy) Run() error {

	p.Router.PathPrefix("/").Handler(p.proxyRouteHandler())
	srv := &http.Server{
		Handler:           p.Router,
		Addr:              ":8679",
		ReadTimeout:       5 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       30 * time.Second,
		ReadHeaderTimeout: 5 * time.Second,
	}

	return srv.ListenAndServe()
}
