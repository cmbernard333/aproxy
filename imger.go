package aproxy

import (
	"errors"
	"image"
	"image/color"
	"image/jpeg" // register jpeg decoder
	"image/png"  // register png decoder
	"io"
)

// Changeable is an interface that allows calling the Set(...) function to change the pixels in an image
type Changeable interface {
	Set(x, y int, c color.Color)
}

// ChangeableYcbcr represents a wrapper interface for Ycbcr that implements the Set function
type ChangeableYcbcr struct {
	*image.YCbCr
}

// Set function for using x,y to convert the color coordinates for a Ycbcr color space
func (cy *ChangeableYcbcr) Set(x, y int, c color.Color) {
	ycbcrColor, _ := color.YCbCrModel.Convert(c).(color.YCbCr)
	yoffset := cy.YOffset(x, y)
	coffset := cy.COffset(x, y)
	// go into the uint8 arrays in the ycbcr and change them using coordinates from the offset
	cy.YCbCr.Y[yoffset] = ycbcrColor.Y
	cy.YCbCr.Cb[coffset] = ycbcrColor.Cb
	cy.YCbCr.Cr[coffset] = ycbcrColor.Cr
}

// ColorConfig contains information about the image
// width, height,
type ColorConfig struct {
	image.Config
	Palette *color.Palette
	format  string
}

// IsPaletted returns whether or not the color config describing an image is paletted
func (c *ColorConfig) IsPaletted() bool {
	return c.Palette != nil
}

// NewColorConfig returns a new ColorConfig describing the given image read in by the io.Reader
// underneath it uses DecodeConfig so readers should be reset before reuse
func NewColorConfig(imageReader io.Reader) (*ColorConfig, error) {
	config, format, err := image.DecodeConfig(imageReader)
	if err != nil {
		return nil, err
	}
	pal, _ := config.ColorModel.(color.Palette)
	return &ColorConfig{
		Config:  config,
		Palette: &pal,
		format:  format,
	}, nil
}

// GrayScaleImageInPlace takes an image loaded by an io.Reader and grayscales it in place
// returns error if the image cannot be decoded or changed
// it does not assume a color palette
// returns a pointer to the changed image
func GrayScaleImageInPlace(imageReader io.Reader) (image.Image, string, error) {
	img, format, err := image.Decode(imageReader)
	if err != nil {
		return nil, "unknown", err
	}

	boundsRec := img.Bounds() // bounding rectangle for the image
	imgc, ok := img.(Changeable)
	if !ok {
		// we need to use the custom interface ycbcr and others that doesn't support Set(...)
		ycbcr, ok := img.(*image.YCbCr)
		if !ok {
			return nil, format, errors.New("unrecognized color space")
		}
		imgc = &ChangeableYcbcr{YCbCr: ycbcr}
	}

	for y := boundsRec.Min.Y; y < boundsRec.Max.Y; y++ {
		for x := boundsRec.Min.X; x < boundsRec.Max.X; x++ {
			// uses color.GrayModel.Convert(coordinates) to convert to gray scale
			// convert to gray scale using weight method described here: https://www.tutorialspoint.com/dip/grayscale_to_rgb_conversion.htm
			colorPixel := img.At(x, y)
			_, _, _, a := colorPixel.RGBA()
			if a != 0 { // check for transparent pixel - this is for PNGs with transparent backgrounds - non-alpha channel image formats (e.g JPEG) won't have this
				grayPixel := color.GrayModel.Convert(colorPixel)
				imgc.Set(x, y, grayPixel)
			}
		}
	}

	return img, format, nil
}

// GrayScaleImage takes an image loaded by an io.Reader and grayscales it
// returns error if the image cannot be decoded or changed
// it does not assume a color palette
// returns an image in RGBA colorspace
func GrayScaleImage(imageReader io.Reader) (image.Image, error) {
	img, _, err := image.Decode(imageReader)
	if err != nil {
		return nil, err
	}

	boundsRec := img.Bounds()           // bounding rectangle for the image
	imgRGBA := image.NewRGBA(boundsRec) // colorspace change to RGBA - produces a new image

	for y := boundsRec.Min.Y; y < boundsRec.Max.Y; y++ {
		for x := boundsRec.Min.X; x < boundsRec.Max.X; x++ {
			// uses color.GrayModel.Convert(coordinates) to convert to gray scale
			// convert to gray scale using weight method described here: https://www.tutorialspoint.com/dip/grayscale_to_rgb_conversion.htm
			colorPixel := img.At(x, y)
			_, _, _, a := colorPixel.RGBA()
			if a != 0 { // check for transparent pixel - this is for PNGs with transparent backgrounds - JPEG won't have this
				grayPixel := color.GrayModel.Convert(colorPixel)
				imgRGBA.Set(x, y, grayPixel)
			}
		}
	}

	return imgRGBA, nil
}

// EncodeImage is a generic function for encoding an image to a writer
// it selects the encoder based on the provided format
// it returns an error if it cannot write or if the format is unspported
func EncodeImage(writer io.Writer, img image.Image, format string) error {
	switch format {
	case "jpeg":
		return jpeg.Encode(writer, img, &jpeg.Options{Quality: 100})
	case "png":
		return png.Encode(writer, img)
	default:
		return errors.New("unsupported encoding")
	}
}
