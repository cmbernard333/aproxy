# Go Proxy Server
* Goal: Create an HTTP proxy server which grayscales images using Go.

aproxy is a simple golang http proxy that does the following:
1. Takes requests at any path prefixed from "/"
2. Attempts to download the path as an image using a provided `-source-addr`
3. Grayscales the image
4. Returns the grayscaled image to the client

## Building
The proxy server can be created by running:
```
make
```
Which produces the executable `aproxy`.

## Manual Testing
It can be run like this:
```
./aproxy -source-addr https://i.imgur.com/
```

Which starts the server listening on `:8679`.

Testing it is as simple as running a `curl` command like this:
```
curl -v -o 1.jpg -w "%{http_code}" http://localhost:8679/p6Gp1ba.jpg
```

The example provided pulls this image:

![Gritty Teenage Mutant Pirate Turtle](https://i.imgur.com/p6Gp1ba.jpg)

And produces the same image in grayscale like this:

![Old timey Gritty Teenage Mutant Pirate Turtle](https://i.imgur.com/dVRtylk.jpg)
