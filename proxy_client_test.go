package aproxy

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProxyCorrectURL(t *testing.T) {

	if testing.Short() {
		t.Skip()
	}

	sourceAddr := "https://i.imgur.com/"
	imagePath := "p6Gp1ba.jpg"
	expectedURLPath := fmt.Sprintf("%s%s", sourceAddr, imagePath)
	p, err := NewProxyClientWithAddr(sourceAddr)
	assert.NoError(t, err, "Could not create proxy client")

	resp, err := p.ProxyGetRequest(imagePath, "127.0.0.1", "http", "127.0.0.1")
	assert.NoError(t, err, "Error on ProxyGetRequest")
	assert.NotNil(t, resp, "Response should not be nil")

	requestURL := resp.Request.URL.String()
	assert.NoError(t, err, "Error finding response location")
	assert.NotNil(t, requestURL, "locationURL should not be nil")

	assert.Truef(t, strings.Compare(expectedURLPath, requestURL) == 0, "expectedURL %s does not equal %s", expectedURLPath, requestURL)

}
