package aproxy

import (
	"net"
	"net/http"
	"net/url"
	"time"
)

// ProxyClient is an http(s) proxy client that makes http requests using the given client as the given source address
type ProxyClient struct {
	SourceURL *url.URL
	Client    *http.Client
}

// NewProxyClientWithAddr returns a new ProxyClient with a default http.Client configured with timeouts.
// the new ProxyClient uses the sourceAddr as the address to forward requests too
// if the sourceAddr is not a proper url an error will be returned
func NewProxyClientWithAddr(sourceAddr string) (*ProxyClient, error) {
	sourceURL, err := url.ParseRequestURI(sourceAddr)
	if err != nil {
		return nil, err
	}
	p := &ProxyClient{
		SourceURL: sourceURL,
		Client: &http.Client{
			Timeout: 15 * time.Second, // covers entire exchange from dial to reading the body
			Transport: &http.Transport{ // handles single http request execution
				DialContext: (&net.Dialer{
					Timeout:   5 * time.Second, // maximum amount of time to wait for a connect to complete
					KeepAlive: 30 * time.Second,
				}).DialContext,
				TLSHandshakeTimeout:   30 * time.Second, // handshake timeout
				ResponseHeaderTimeout: 10 * time.Second, // handling the response timeout
				ExpectContinueTimeout: 1 * time.Second,  // timeout for response 100-continue
			},
		},
	}
	return p, nil
}

// NewProxyClientWithAddrAndClient returns a new ProxyClient using the given http client
// if the sourceAddr provided is not a proper url an error will be returned
func NewProxyClientWithAddrAndClient(sourceAddr string, client *http.Client) (*ProxyClient, error) {
	sourceURL, err := url.ParseRequestURI(sourceAddr)
	if err != nil {
		return nil, err
	}
	p := &ProxyClient{
		SourceURL: sourceURL,
		Client:    client,
	}
	return p, nil
}

// ProxyGetRequest takes the given path and sends it to the configured SourceAddr
func (p *ProxyClient) ProxyGetRequest(path, forwardedFor, proto, realIP string) (resp *http.Response, err error) {
	p.SourceURL.Path = path
	request, err := http.NewRequest("GET", p.SourceURL.String(), nil)
	if err != nil {
		return nil, err
	}
	// headers referenced from here: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
	request.Header[http.CanonicalHeaderKey("X-Forwarded-For")] = []string{forwardedFor}
	request.Header[http.CanonicalHeaderKey("X-Forwarded-Proto")] = []string{proto}
	request.Header[http.CanonicalHeaderKey("X-Real-Ip")] = []string{realIP}

	return p.Client.Do(request)
}
